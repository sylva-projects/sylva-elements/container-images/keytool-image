FROM alpine:3.21.3

ENV TARGETOS "linux"
ENV TARGETARCH "amd64"
# renovate: datasource=github-tags depName=kubernetes/kubectl VersionTemplate=kubernetes-
ENV KUBECTL_VERSION "1.31.2"
# renovate: datasource=github-releases depName=mikefarah/yq VersionTemplate=v
ENV YQ_VERSION "4.45.1"

# hadolint ignore=DL3018, SC2028
RUN apk update \
    && apk --no-cache add bash wget openjdk21 \
    && wget -q --show-progress --progress=bar:force https://dl.k8s.io/release/v${KUBECTL_VERSION}/bin/${TARGETOS}/${TARGETARCH}/kubectl -O /usr/local/bin/kubectl \
    && wget -q --show-progress --progress=bar:force https://github.com/mikefarah/yq/releases/download/v${YQ_VERSION}/yq_${TARGETOS}_${TARGETARCH} -O /usr/local/bin/yq \
    && chmod +x /usr/local/bin/kubectl /usr/local/bin/yq \
    && kubectl version --client \
    && yq --version \
    && echo "KUBECTL_VERSION:${KUBECTL_VERSION}\nYQ_VERSION:${YQ_VERSION}\n" > /keytool-image-version \
    && cat keytool-image-version

COPY env /usr/local/bin
