# keytool-image

The provided Dockerfiles build a lightweight Alpine Linux-based Docker image with various Kubernetes-related tools
and keytool in order to create truststore and keystores files. 
It sets up the environment with specified tool versions, installs dependencies, downloads tools, and verifies their versions. The resulting image includes the tools and relevant environment information in a version file. The CMD instruction creates a compressed archive of the tools and version information when the container is running.

In the Sylva project, the Renovate bot tool is integrated into GitLab CI to assist us in maintaining the images in an easier way.

## Image includes the following tools:

1. **openjdk**
    - `openjdk` is the official command-line tool for interacting with Java. It allows users to perform 
    various operations, such as create a truststore or keystore using `keytool` library.

2. **kubectl (Kubernetes Control)**
   - `kubectl` is the official command-line tool for interacting with Kubernetes clusters. It allows users to perform various operations, such as deploying applications, inspecting cluster resources, managing configurations, and troubleshooting issues within a Kubernetes cluster.

3. **Yq (YAML Processor and Query Tool)**
   - `Yq` is a lightweight and flexible command-line tool for processing and querying YAML files. It allows users to perform various operations on YAML data, such as filtering, updating, and extracting information. Yq is particularly useful in the context of Kubernetes for working with YAML manifests and configuration files.
